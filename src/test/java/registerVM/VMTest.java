package registerVM;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import vm.CompileException;
import vm.Register;
import vm.VM;

public class VMTest {
	private VM vm;

	@Before
	public void init() {
		vm = new VM();
	}

	public void assertRegister(String letter, int value) {
		Register reg = vm.getRegister(letter);
		assertEquals(value, reg.retreive());
	}

	@Test
	public void test1() throws CompileException {
		vm.execute(
			"section code "
			+ "f1: "
				+ "mov a 5 "
				+ "mov b a "
				+ "ret "
			+ "f2: "
				+ "cmp a 5 "
				+ "jeq l1 "
				+ "jlt l2 "
				+ "jgt l3 "
				+ "jmp l4 "
				+ "ret "
				+ "l1: "
					+ "mov c 1 "
					+ "out c "
					+ "ret "
				+ "l2: "
					+ "mov c 2 "
					+ "out c "
					+ "ret "
				+ "l3: "
					+ "mov c 3 "
					+ "out c "
					+ "ret "
				+ "l4: "
					+ "mov c 4 "
					+ "out c "
					+ "ret "
			+ "_start: "
				+ "call f1 "
				+ "call f2 "
				+ "add b 4 "
				+ "out b "
				+ "sub a 3 " 
				+ "mul b 2 " 
				+ "div b 6 "
				+ "ret"
		);
		assertRegister("a", 2);
		assertRegister("b", 3);
		assertRegister("c", 1);
	}
	
	@Test
	public void test2() throws CompileException {
		vm.execute(
			"section data "
					+ "X 5 "
					+ "Y 6 "
			+ "section code "
			+ "_start: "
				+ "mov a X "
				+ "mov b Y "
				+ "add a b "
				
				+ "ret"
		);
		assertRegister("a", 11);
		assertRegister("b", 6);
	}
	
	@Test
	public void test3() throws CompileException {
		vm.execute(
			"section code "
			+ "_start: "
				+ "mov a X "
				+ "mov b Y "
				+ "add a b "
				+ "ret "
			+ "section data "
				+ "X 5 "
				+ "Y 6 "
		);
		assertRegister("a", 11);
		assertRegister("b", 6);
	}
	
	@Test
	public void testOverflow() throws CompileException {
		vm.execute(
			"section code "
			+ "f1: "
				+ "cmp ovf 1 "
				+ "jeq l1 "
				+ "jne l2 "
				+ "l1: "
					+ "mov c 1 "
					+ "ret "
				+ "l2: "
					+ "mov d 1 "
					+ "ret "
			+ "_start: "
				+ "mov a BIG "
				+ "add a 5 "
				+ "call f1 "
				+ "mov a NOT_SOBIG "
				+ "add a 5 "
				+ "call f1 "
				+ "ret "
			+ "section data "
				+ "BIG 2147483647 "
				+ "NOT_SOBIG 2147483627 "
				
		);
		assertRegister("c", 1);
		assertRegister("d", 1);
	}

}
