package registerVM;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import instructions.*;
import instructions.flow.InstructionCall;
import vm.CompileException;
import vm.Compiler;
import vm.IntRegister;
import vm.Lexer;
import vm.Token;
import vm.VM;

public class CompilerTest {
	private VM vm;
	private Lexer parser;
	private Compiler compiler;

	@Before
	public void init() {
		vm = new VM();
		parser = new Lexer();
		compiler = new Compiler(vm);
	}

	public void testString(String program, Instruction[] checkInstructions) throws CompileException {
		ArrayList<Token> tokens = parser.tokenize(program);
		ArrayList<Instruction> instr = compiler.compile(tokens);
		Instruction[] instructions = instr.toArray(new Instruction[1]);
		assertEquals(instructions.length, checkInstructions.length);
		for (int i = 0; i < instructions.length; i++) {
			Instruction first = instructions[i];
			Instruction second = checkInstructions[i];
			assertEquals(first, second);
		}
	}
	
	private IntRegister makeReg(int id, String name ) {
		return new IntRegister(id + VM.REG_OFFSET, name);
	}
	
	@Test
	public void test() throws CompileException {
		testString(
				"section code " +
						"_start: " + 
				"mov a	 5\n" +
				"mov b	 a\n" +
				"ret "+
					"f: " + 
				"call _start "+
				"add a	 4\n" +
				"sub b	 7\n" +
				"mul a	 2\n" +
				"div  b	 6\n" +
				"ret "+
					"f2: " + 
				"add a	 b\n" +
				"sub b	 a\n" +
				"mul a	 b\n" +
				"div  b	 a\n" +
				"ret ",
				new Instruction[] {
						new InstructionMovInt(makeReg(0, "a"), 0, 2),
						new InstructionMovReg(makeReg(1, "b"), makeReg(0, "a"), 3),
						new InstructionRet(4),
						new InstructionCall("_start", -1, 6),
						new InstructionAddInt(makeReg(0,"a"), 1, 7),
						new InstructionSubInt(makeReg(1, "b"), 2, 8),
						new InstructionMulInt(makeReg(0,"a"), 3, 9),
						new InstructionDivInt(makeReg(1, "b"), 4, 10),
						new InstructionRet(11),
						new InstructionAddReg(makeReg(0,"a"), makeReg(1, "b"), 13),
						new InstructionSubReg(makeReg(1, "b"), makeReg(0, "a"), 14),
						new InstructionMulReg(makeReg(0,"a"), makeReg(1, "b"), 15),
						new InstructionDivReg(makeReg(1, "b"), makeReg(0, "a"), 16),
						new InstructionRet(17)
				}
		);
	}

	@Test (expected = CompileException.class)
	public void test2() throws CompileException {
		testString(
				"section code " +
				"_start: " + 
				"call f ",
				new Instruction[] {
						new InstructionCall("_start", -1),
				}
		);
	}
	@Test (expected = CompileException.class)
	public void test3() throws CompileException {
		testString(
				" code " +
				"_start: " + 
				"call f ",
				new Instruction[] {
						new InstructionCall("_start", -1),
				}
		);
	}
	
	@Test (expected = CompileException.class)
	public void test4() throws CompileException {
		testString(
				"section code " +
				"move a 4",
				new Instruction[] {
						new InstructionMovInt(makeReg(0, "a"), 4, 1),
				}
		);
	}
}
