package instructions.flow;

public class InstructionJLE extends ConditionalJumpInstruction {

	public InstructionJLE(String aLabel, int line) {
		super(aLabel, line);
	}

	public InstructionJLE(String lable, int index, int line) {
		super(lable, index, line);
	}

	@Override
	protected boolean isJumpEnabled(int value) {
		if (value <= 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJLE)) {
			return false;
		}
		InstructionJLE instr = (InstructionJLE) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
