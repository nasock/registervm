package instructions.flow;

import instructions.*;

public abstract class FlowInstruction extends Instruction {
	protected String label;
	protected int proсIndex;
	
	public FlowInstruction(String aLabel, int index, int line) {
		label = aLabel;
		proсIndex = index;
		putLine(line);
	}
	
	public FlowInstruction(String aLabel, int line) {
		this(aLabel, -2, line);
	}
	
	public String getLabel() {
		return label;
	}
	
	public void updateIndex(int index) {
		proсIndex = index;
	}

}
