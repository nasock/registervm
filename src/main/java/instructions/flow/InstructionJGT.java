package instructions.flow;

public class InstructionJGT extends ConditionalJumpInstruction {

	public InstructionJGT(String aLabel, int line) {
		super(aLabel, line);
	}
	
	public InstructionJGT(String lable, int index, int line) {
		super(lable, index, line);
	}
	
	@Override
	protected boolean isJumpEnabled(int value) {
		if(value > 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJGT)) {
			return false;
		}
		InstructionJGT instr = (InstructionJGT) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
