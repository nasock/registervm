package instructions.flow;

import vm.Register;
import vm.VM;

public class InstructionJMP extends FlowInstruction {

	public InstructionJMP(String aLabel, int line) {
		super(aLabel, line);
	}
	
	public InstructionJMP(String lable, int index, int line) {
		super(lable, index, line);
	}

	@Override
	public void execute(VM vm) {
		Register ip = vm.getIP();
		ip.store(proсIndex);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJMP)) {
			return false;
		}
		InstructionJMP instr = (InstructionJMP) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
