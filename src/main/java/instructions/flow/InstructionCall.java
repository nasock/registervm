package instructions.flow;

import vm.*;

public class InstructionCall extends FlowInstruction {

	public InstructionCall(String lable, int line) {
		super(lable, line);
	}
	
	public InstructionCall(String lable, int index, int line) {
		super(lable, index, line);
	}

	@Override
	public void execute(VM vm) {
		Register ip = vm.getIP();
		Register ra = vm.getRA();
		ra.store(ip);
		ip.store(proсIndex);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionCall)) {
			return false;
		}
		InstructionCall instr = (InstructionCall) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
