package instructions.flow;

public class InstructionJNE extends ConditionalJumpInstruction {

	public InstructionJNE(String aLabel, int line) {
		super(aLabel, line);
	}

	public InstructionJNE(String lable, int index, int line) {
		super(lable, index, line);
	}
	
	@Override
	protected boolean isJumpEnabled(int value) {
		if(value != 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJNE)) {
			return false;
		}
		InstructionJNE instr = (InstructionJNE) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
