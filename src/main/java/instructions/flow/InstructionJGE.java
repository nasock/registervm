package instructions.flow;

public class InstructionJGE extends ConditionalJumpInstruction {

	public InstructionJGE(String aLabel, int line) {
		super(aLabel, line);
	}

	public InstructionJGE(String lable, int index, int line) {
		super(lable, index, line);
	}
	
	@Override
	protected boolean isJumpEnabled(int value) {
		if(value >= 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJGE)) {
			return false;
		}
		InstructionJGE instr = (InstructionJGE) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
