package instructions.flow;

public class InstructionJLT extends ConditionalJumpInstruction {

	public InstructionJLT(String aLabel, int line) {
		super(aLabel, line);
	}

	public InstructionJLT(String lable, int index, int line) {
		super(lable, index, line);
	}
	
	@Override
	protected boolean isJumpEnabled(int value) {
		if(value < 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJLT)) {
			return false;
		}
		InstructionJLT instr = (InstructionJLT) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
