package instructions.flow;

import vm.Register;
import vm.VM;

public abstract class ConditionalJumpInstruction extends FlowInstruction {
	
	public ConditionalJumpInstruction(String aLabel,int line) {
		super(aLabel, line);
	}

	public ConditionalJumpInstruction(String lable, int index, int line) {
		super(lable, index, line);
	}

	protected abstract boolean isJumpEnabled(int value);
	
	@Override
	public void execute(VM vm) {
		Register ds = vm.getDS();
		if (!isJumpEnabled(ds.retreive())) {
			return;
		}
		Register ip = vm.getIP();
		ip.store(proсIndex);
	}

}
