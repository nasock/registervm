package instructions.flow;


public class InstructionJEQ extends ConditionalJumpInstruction {

	public InstructionJEQ(String aLabel, int line) {
		super(aLabel, line);
	}

	public InstructionJEQ(String lable, int index, int line) {
		super(lable, index, line);
	}
	
	@Override
	protected boolean isJumpEnabled(int value) {
		if (value == 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionJEQ)) {
			return false;
		}
		InstructionJEQ instr = (InstructionJEQ) obj;
		return instr.label.equals(label) && instr.proсIndex == proсIndex;
	}

}
