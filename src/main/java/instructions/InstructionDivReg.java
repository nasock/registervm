package instructions;

import vm.Register;
import vm.VM;

public class InstructionDivReg extends Instruction {
	private Register registerInput;
	private Register registerOutput;

	public InstructionDivReg(Register regInput, Register regOutput, int line) {
		registerInput = regInput;
		registerOutput = regOutput;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regInputValue = registerInput.retreive();
		int regOutputValue = registerOutput.retreive();
		registerInput.store(regInputValue / regOutputValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionDivReg)) {
			return false;
		}
		InstructionDivReg instr = (InstructionDivReg) obj;
		return instr.registerInput.equals(registerInput) && instr.registerOutput.equals(registerOutput);
	}

}
