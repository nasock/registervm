package instructions;

import vm.Register;
import vm.VM;

public class InstructionAddInt extends Instruction {
	private Register register;
	private int index;

	public InstructionAddInt(Register aRegister, int aIndex, int line) {
		register = aRegister;
		index = aIndex;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regValue = register.retreive();
		int value = vm.getInt(index);
		Register ovf = vm.getOVF();
		try {
			int result = Math.addExact(regValue, value);
			ovf.store(0);
			register.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			register.store(regValue + value);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionAddInt)) {
			return false;
		}
		InstructionAddInt instr = (InstructionAddInt) obj;
		return instr.register.equals(register) && instr.index == index;
	}

}
