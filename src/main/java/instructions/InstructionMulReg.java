package instructions;

import vm.Register;
import vm.VM;

public class InstructionMulReg extends Instruction {
	private Register registerInput;
	private Register registerOutput;

	public InstructionMulReg(Register regInput, Register regOutput, int line) {
		registerInput = regInput;
		registerOutput = regOutput;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regInputValue = registerInput.retreive();
		int regOutputValue = registerOutput.retreive();
		Register ovf = vm.getOVF();
		try {
			int result = Math.multiplyExact(regInputValue, regOutputValue);
			ovf.store(0);
			registerInput.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			registerInput.store(regInputValue * regOutputValue);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionMulReg)) {
			return false;
		}
		InstructionMulReg instr = (InstructionMulReg) obj;
		return instr.registerInput.equals(registerInput) && instr.registerOutput.equals(registerOutput);
	}
	
}
