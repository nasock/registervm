package instructions;

import vm.*;

public class InstructionSubReg extends Instruction {
	private Register registerInput;
	private Register registerOutput;

	public InstructionSubReg(Register regInput, Register regOutput, int line) {
		registerInput = regInput;
		registerOutput = regOutput;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regInputValue = registerInput.retreive();
		int regOutputValue = registerOutput.retreive();
		Register ovf = vm.getOVF();
		try {
			int result = Math.subtractExact(regInputValue, regOutputValue);
			ovf.store(0);
			registerInput.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			registerInput.store(regInputValue - regOutputValue);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionSubReg)) {
			return false;
		}
		InstructionSubReg instr = (InstructionSubReg) obj;
		return instr.registerInput.equals(registerInput) && instr.registerOutput.equals(registerOutput);
	}

}
