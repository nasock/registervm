package instructions;

import vm.VM;

public class InstructionBRK extends Instruction {

	public InstructionBRK(int line) {
		putLine(line);
	}
	
	@Override
	public void execute(VM vm) {
		vm.debug();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionBRK)) {
			return false;
		}
		return true;
	}

}
