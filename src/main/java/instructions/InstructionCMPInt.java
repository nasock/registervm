package instructions;

import vm.Register;
import vm.VM;

public class InstructionCMPInt extends Instruction {
	private Register register;
	private int index;

	public InstructionCMPInt(Register reg, int ind, int line) {
		register = reg;
		index = ind;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		Register ds = vm.getDS();
		int regValue = register.retreive();
		int value = vm.getInt(index);
		Register ovf = vm.getOVF();
		try {
			int result = Math.subtractExact(regValue, value);
			ovf.store(0);
			ds.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			ds.store(regValue - value);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionCMPInt)) {
			return false;
		}
		InstructionCMPInt instr = (InstructionCMPInt) obj;
		return instr.register.equals(register) && instr.index == index;
	}

}
