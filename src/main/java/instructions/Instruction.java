package instructions;

import vm.VM;

public abstract class Instruction {
	private int line;

	public void putLine(int aLine) {
		line = aLine;
	}

	public int getLine() {
		return line;
	}

	public abstract void execute(VM vm);

	public abstract boolean equals(Object obj);
}
