package instructions;

import vm.Register;
import vm.VM;

public class InstructionOut extends Instruction {
	private Register register;

	public InstructionOut(Register reg, int line) {
		register = reg;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		System.out.println(register.retreive());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionOut)) {
			return false;
		}
		InstructionOut instr = (InstructionOut) obj;
		return instr.register.equals(register);
	}

}
