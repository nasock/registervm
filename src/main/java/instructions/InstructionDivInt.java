package instructions;

import vm.Register;
import vm.VM;

public class InstructionDivInt extends Instruction {
	private Register register;
	private int index;

	public InstructionDivInt(Register reg, int ind, int line) {
		register = reg;
		index = ind;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regValue = register.retreive();
		int value = vm.getInt(index);
		register.store(regValue / value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionDivInt)) {
			return false;
		}
		InstructionDivInt instr = (InstructionDivInt) obj;
		return instr.register.equals(register) && instr.index == index;
	}

}
