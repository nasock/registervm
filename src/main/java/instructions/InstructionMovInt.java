package instructions;

import vm.Register;
import vm.VM;

public class InstructionMovInt extends Instruction {
	private Register register;
	private int index;

	public InstructionMovInt(Register reg, int ind, int line) {
		register = reg;
		index = ind;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int value = vm.getInt(index);
		register.store(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionMovInt)) {
			return false;
		}
		InstructionMovInt instr = (InstructionMovInt) obj;
		return instr.register.equals(register) && instr.index == index;
	}
}
