package instructions;

import vm.Register;
import vm.VM;

public class InstructionMovReg extends Instruction {
	private Register registerInput;
	private Register registerOutput;

	public InstructionMovReg(Register regInput, Register regOutput, int line) {
		registerInput = regInput;
		registerOutput = regOutput;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regOutputValue = registerOutput.retreive();
		registerInput.store(regOutputValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionMovReg)) {
			return false;
		}
		InstructionMovReg instr = (InstructionMovReg) obj;
		return instr.registerInput.equals(registerInput) && instr.registerOutput.equals(registerOutput);
	}

}
