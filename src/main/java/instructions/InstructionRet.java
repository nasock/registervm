package instructions;

import vm.Register;
import vm.VM;

public class InstructionRet extends Instruction {
	
	
	public InstructionRet(int line) {
		putLine(line);
	}
	
	@Override
	public void execute(VM vm) {
		Register ip = vm.getIP(); 
		Register ra = vm.getRA(); 
		ip.store(ra);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionRet)) {
			return false;
		}
		return true;
	}

}
