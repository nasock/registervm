package instructions;

import vm.Register;
import vm.VM;

public class InstructionCMPReg extends Instruction {
	private Register register1;
	private Register register2;

	public InstructionCMPReg(Register reg1, Register reg2, int line) {
		register1 = reg1;
		register2 = reg2;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		Register ds = vm.getDS();
		Register ovf = vm.getOVF();
		int reg1Value = register1.retreive();
		int reg2Value = register2.retreive();
		try {
			int result = Math.subtractExact(reg1Value, reg2Value);
			ovf.store(0);
			ds.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			ds.store(reg1Value - reg2Value);
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionCMPReg)) {
			return false;
		}
		InstructionCMPReg instr = (InstructionCMPReg) obj;
		return instr.register1.equals(register1) && instr.register2.equals(register2);
	}

}
