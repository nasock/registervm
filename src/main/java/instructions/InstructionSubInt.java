package instructions;

import vm.Register;
import vm.VM;

public class InstructionSubInt extends Instruction {
	private Register register;
	private int index;

	public InstructionSubInt(Register reg, int ind, int line) {
		register = reg;
		index = ind;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regValue = register.retreive();
		int value = vm.getInt(index);
		Register ovf = vm.getOVF();
		try {
			int result = Math.subtractExact(regValue, value);
			ovf.store(0);
			register.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			register.store(regValue - value);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionSubInt)) {
			return false;
		}
		InstructionSubInt instr = (InstructionSubInt) obj;
		return instr.register.equals(register) && instr.index == index;
	}

}
