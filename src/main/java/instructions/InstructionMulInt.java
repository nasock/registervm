package instructions;

import vm.Register;
import vm.VM;

public class InstructionMulInt extends Instruction {
	private Register register;
	private int index;

	public InstructionMulInt(Register reg, int ind, int line) {
		register = reg;
		index = ind;
		putLine(line);
	}

	@Override
	public void execute(VM vm) {
		int regValue = register.retreive();
		int value = vm.getInt(index);
		Register ovf = vm.getOVF();
		try {
			int result = Math.multiplyExact(regValue, value);
			ovf.store(0);
			register.store(result);
		} catch (ArithmeticException e) {
			ovf.store(1);
			register.store(regValue * value);
		}
		
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof InstructionMulInt)) {
			return false;
		}
		InstructionMulInt instr = (InstructionMulInt) obj;
		return instr.register.equals(register) && instr.index == index;
	}
}
