package vm;

import java.util.ArrayList;
import java.util.HashMap;

public class Lexer {
	private HashMap<String, Token.TokenType> reservedWords;

	public Lexer() {
		reservedWords = new HashMap<String, Token.TokenType>();
		reservedWords.put("section", Token.TokenType.SECTION);
		reservedWords.put("data", Token.TokenType.DATA);
		reservedWords.put("code", Token.TokenType.CODE);
		reservedWords.put("brk", Token.TokenType.BRK);
		reservedWords.put("call", Token.TokenType.CALL);
		reservedWords.put("mov", Token.TokenType.MOV);
		reservedWords.put("add", Token.TokenType.ADD);
		reservedWords.put("sub", Token.TokenType.SUB);
		reservedWords.put("mul", Token.TokenType.MUL);
		reservedWords.put("div", Token.TokenType.DIV);
		reservedWords.put("ret", Token.TokenType.RET);
		reservedWords.put("out", Token.TokenType.OUT);
		reservedWords.put("cmp", Token.TokenType.CMP);
		reservedWords.put("jmp", Token.TokenType.JMP);
		reservedWords.put("jeq", Token.TokenType.JEQ);
		reservedWords.put("jne", Token.TokenType.JNE);
		reservedWords.put("jge", Token.TokenType.JGE);
		reservedWords.put("jgt", Token.TokenType.JGT);
		reservedWords.put("jle", Token.TokenType.JLE);
		reservedWords.put("jlt", Token.TokenType.JLT);
	}

	private int findCommentEnd(char[] chars, int i) {
		char ch = chars[i];
		while (ch != '\n') {
			i++;
			if (i > chars.length - 1) {
				break;
			}
			ch = chars[i];
		}
		return i;
	}

	private int findWordStart(char[] chars, int i) {
		char ch = chars[i];
		while (ch == ' ' || ch == '\t') {
			i++;
			if (i > chars.length - 1) {
				break;
			}
			ch = chars[i];
		}
		return i;
	}

	private int findWordEnd(char[] chars, int i) {
		char ch = chars[i];
		while (!(ch == ' ' || ch == '\t' || ch == '\n')) {
			i++;
			if (i > chars.length - 1) {
				break;
			}
			ch = chars[i];
		}
		return i;
	}

	private String makeWord(char[] chars, int startIndex, int endIndex) {
		int length = endIndex - startIndex;
		char[] newChars = new char[length];
		for (int i = 0; i < length; i++) {
			newChars[i] = chars[startIndex + i];
		}
		return new String(newChars);
	}

	private Token makeToken(char[] chars, int startIndex, int endIndex, int lineCounter) {
		String str = makeWord(chars, startIndex, endIndex);
		Token.TokenType type = reservedWords.get(str);
		if (type == null) {
			if (str.substring(str.length() - 1, str.length()).equals(":")) {
				type = Token.TokenType.PROC;
			} else if (str.matches("[A-Z][A-Z?a-z?0-9?_?]*")) {
				type = Token.TokenType.CONST;
			} else if (str.matches("[+-]?[0-9]+")) {
				type = Token.TokenType.INT;
			} else {
				type = Token.TokenType.NAME;
			}
		}
		return new Token(type, str, lineCounter);
	}

	public ArrayList<Token> tokenize(String string) {
		string = string.trim();
		ArrayList<Token> tokens = new ArrayList<Token>();
		char[] chars = string.toCharArray();
		int i = 0;
		int startIndex = 0;
		int endIndex = 0;
		int lineCounter = 0;
		while (i < chars.length) {
			char ch = chars[i];
			if (ch == ';') {
				i = findCommentEnd(chars, i);
			} else if (ch == '\n') {
				lineCounter++;
				i++;
			} else if (ch == ' ' || ch == '\t') {
				i = findWordStart(chars, i);
				startIndex = i;
				i = findWordEnd(chars, i);
				endIndex = i;
				if (startIndex != endIndex) {
					Token token = makeToken(chars, startIndex, endIndex, lineCounter);
					tokens.add(token);
				}
			} else {
				startIndex = i;
				i = findWordEnd(chars, i);
				endIndex = i;
				if (startIndex != endIndex) {
					Token token = makeToken(chars, startIndex, endIndex, lineCounter);
					tokens.add(token);
				}
			}
		}

		return tokens;
	}

//	private static String readFile(String file) throws IOException {
//		BufferedReader reader = new BufferedReader(new FileReader(file));
//		String line = null;
//		StringBuilder stringBuilder = new StringBuilder();
//
//		try {
//			while ((line = reader.readLine()) != null) {
//				stringBuilder.append(line);
//				stringBuilder.append("\n");
//			}
//
//			return stringBuilder.toString();
//		} finally {
//			reader.close();
//		}
//	}
//
//	public static void main(String[] args) {
//		String fileName = args[0];
//		Lexer l = new Lexer();
//		try {
//			String program = readFile(fileName);
//			ArrayList<Token> tokens = l.tokenize(program);
//			for (Token t : tokens) {
//				System.out.println(t);
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

}
