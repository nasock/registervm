package vm;

public class VMRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VMRuntimeException(String cause) {
		super(cause);
	}
}
