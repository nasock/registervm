package vm;

import java.util.ArrayList;

public class StackRegister extends Register {
	private ArrayList<Integer> values;
	public final static int STACK_EMPTY = -2;

	public StackRegister(int aId, String name) {
		super(aId, name);
		values = new ArrayList<Integer>();
	}

	@Override
	public void store(int v) {
		values.add(v);
	}

	@Override
	public int retreive() {
		if (values.size() == 0) {
			return STACK_EMPTY;
		}
		return values.remove(values.size() - 1);
	}
}
