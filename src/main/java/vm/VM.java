package vm;

import java.util.ArrayList;
import java.util.HashMap;

import instructions.Instruction;

public class VM {
	public enum State {
		FINISHED, RUNNING, DEBUG;
	}

	private String program;
	private Register[] registers;
	private Table table;
	private HashMap<String, Integer> constToIndex;
	private State state;
	private HashMap<String, Integer> regNameToIndex;
	private Debuger debuger;
	private ArrayList<Instruction> instructions;
	private IntRegister ip;
	public final static int REG_OFFSET = 4;
	public final static String START = "_start";

	public VM() {
		regNameToIndex = new HashMap<String, Integer>();
		regNameToIndex.put("ra", 0);
		regNameToIndex.put("ip", 1);
		regNameToIndex.put("ds", 2);
		regNameToIndex.put("ovf", 3);
		registers = new Register[30];
		registers[0] = new StackRegister(0, "ra");
		for (int i = 1; i < registers.length; i++) {
			String name;
			if (i > 3) {
				int pos = i - 4 + 'a';
				Character ch = (char) pos;
				name = ch.toString();
			} else {
				if (i == 0) {
					name = "ra";
				} else if (i == 1) {
					name = "ip";
				} else if (i == 2) {
					name = "ds";
				} else {
					name = "ovf";
				}
			}
			registers[i] = new IntRegister(i, name);
		}
		table = new Table();
		constToIndex = new HashMap<String, Integer>();
	}

	public Register getRA() {
		return registers[0];
	}

	public Register getIP() {
		return registers[1];
	}

	public Register getDS() {
		return registers[2];
	}

	public Register getOVF() {
		return registers[3];
	}

	public Register getRegister(int index) {
		return registers[index];
	}

	public Register getRegister(String name) {
		if (name.matches("[a-z]")) {
			char[] chs = name.toCharArray();
			char ch = chs[0];
			int pos = ch - 'a';
			if (pos < 0 || pos > registers.length - REG_OFFSET) {
				throw new VMRuntimeException("invalid register " + name);
			}
			return registers[pos + REG_OFFSET];
		} else {
			Integer index = regNameToIndex.get(name);
			if (index == null) {
				throw new VMRuntimeException("invalid register " + name);
			}
			return registers[index];
		}
	}

	public int putInt(int value) {
		return table.put(value);
	}

	public int getInt(int index) {
		return table.get(index);
	}

	public void putConst(String name, int value) {
		int index = table.put(value);
		constToIndex.put(name, index);
	}

	public int getConstIndex(String name) {
		Integer index = constToIndex.get(name);
		if (index == null) {
			throw new VMRuntimeException("invalid const name " + name);
		}
		return index;
	}

	public void continueRuning() {
		state = State.RUNNING;
	}

	public void debug() {
		state = State.DEBUG;
	}

	public void exit() {
		state = State.FINISHED;
	}

	public void executeInstruction() {
		Instruction instruction = instructions.get(ip.retreive());
		instruction.execute(this);
		ip.increment();
		if (ip.retreive() < 0) {
			exit();
		}
	}
	
	public String getProgramText() {
		return program;
	}

	public int getInstructionLine() {
		Instruction instruction = instructions.get(ip.retreive());
		return instruction.getLine();
	}

	private void run() {
		continueRuning();
		while (state != State.FINISHED) {
			if (state == State.RUNNING) {
				executeInstruction();
			} else if (state == State.DEBUG) {
				debuger.executeAction(this);
			}
		}
	}

	public void execute(String program) throws CompileException {
		debuger = new Debuger(program);
		Lexer parser = new Lexer();
		ArrayList<Token> tokens = parser.tokenize(program);
		Compiler compiler = new Compiler(this);
		instructions = compiler.compile(tokens);
		HashMap<String, Integer> procsAddresses = compiler.getProcsAddresses();

		int ipIndex = procsAddresses.get(START) + 1;
		ip = (IntRegister) getIP();
		ip.store(ipIndex);
		run();
	}

}
