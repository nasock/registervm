package vm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Debuger {
	private BufferedReader reader;
	private String[] lines;
	private int numToPrint;

	public Debuger(String program) {
		InputStreamReader isr = new InputStreamReader(System.in);
		reader = new BufferedReader(isr);
		lines = program.split("\n");
		for (int i = 0; i < lines.length; i++) {
			String str = i+ ". " + lines[i].trim();
			lines[i] = str;
		}
		numToPrint = 0;
	}

	private ArrayList<String> parseInput(String string) {
		String[] stringArray = string.split(" ");
		ArrayList<String> commands = new ArrayList<String>();
		for (String st : stringArray) {
			st = st.trim();
			if (st.length() == 0) {
				continue;
			}
			commands.add(st);
		}
		return commands;
	}

	private void printLines(VM vm) {
		int lineInd = vm.getInstructionLine();
		StringBuilder strb = new StringBuilder();
		int index = lineInd - numToPrint;
		strb.append("----\n");
		while (index <= lineInd + numToPrint && index < lines.length) {
			if (index > 0) {
				strb.append(lines[index] + "\n");
			}
			index++;
		}
		strb.append("----");
		System.out.println(strb);
	}

	private void exit(VM vm) {
		vm.exit();
	}

	private void continueRuning(VM vm) {
		vm.continueRuning();
	}

	private void step(VM vm) {
		vm.executeInstruction();
	}

	private void printRegister(String name, VM vm) {
		try {
			System.out.println(vm.getRegister(name).toFullString());
		} catch (VMRuntimeException e) {
			System.out.println("invalid register name " + name);
		}
	}

	public void executeAction(VM vm) {
		printLines(vm);
		try {
			String line = reader.readLine();
			ArrayList<String> commands = parseInput(line);
			if (commands.size() == 0) {
				System.out.println("no command");
				return;
			}
			String command = commands.get(0);
			if (command.equals("s")) {
				step(vm);
			} else if (command.equals("c")) {
				continueRuning(vm);
			} else if (command.equals("e")) {
				exit(vm);
			} else if (command.equals("w")) {
				if (commands.size() < 2) {
					System.out.println("no print value");
					return;
				}
				String intStr = commands.get(1);
				try {
					numToPrint = Integer.parseInt(intStr);
				} catch (NumberFormatException e) {
					System.out.println("invalid integer format");
					return;
				}
			} else if (command.equals("r")) {
				if (commands.size() < 2) {
					System.out.println("no register name");
					return;
				}
				String name = commands.get(1);
				printRegister(name, vm);
			}

		} catch (IOException e) {
			System.out.println("debug: IOException " + e.toString());
		}
	}

}
