package vm;

public class Token {
	public enum TokenType {
		SECTION, DATA, CODE, PROC, BRK, 
		CALL, MOV, ADD, SUB, MUL, DIV, CMP, RET, OUT, 
		JMP, JEQ, JNE, JGE, JGT, JLE, JLT,
		INT, NAME, CONST;
	}

	private TokenType type;
	private String value;
	private int line;

	public Token(TokenType aType, String aValue, int aLine) {
		type = aType;
		value = aValue;
		line = aLine;
	}

	public TokenType getType() {
		return type;
	}

	public String getValue() {
		return value;
	}
	
	public int getLine() {
		return line;
	}

	@Override
	public String toString() {
		return line + ". " + type + " : " + value;
	}
}
