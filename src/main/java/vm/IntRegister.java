package vm;

public class IntRegister extends Register {
	private int value;

	public IntRegister(int aId, String name) {
		super(aId, name);
	}

	public void increment() {
		value++;
	}

	@Override
	public void store(int v) {
		value = v;
	}

	@Override
	public int retreive() {
		return value;
	}

}
