package vm;

import java.util.ArrayList;
import java.util.HashMap;

public class Table {
	private HashMap<Integer, Integer> valueToIndex;
	private ArrayList<Integer> integers;

	public Table() {
		valueToIndex = new HashMap<Integer, Integer>();
		integers = new ArrayList<Integer>();
	}

	public int put(int value) {
		int index;
		if (!valueToIndex.containsKey(value)) {
			integers.add(value);
			index = integers.size() - 1;
			valueToIndex.put(value, index);
		} else {
			index = valueToIndex.get(value);
		}
		return index;
	}

	public int get(int index) {
		if (index < 0 || index >= integers.size()) {
			throw new VMRuntimeException("out of bounds");
		}
		return integers.get(index);
	}

}
