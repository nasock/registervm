package vm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class App {
	
	private String readFile(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append("\n");
			}

			return stringBuilder.toString();
		} finally {
			reader.close();
		}
	}

	public static void main(String[] args) {
		String fileName = args[0];
		App app = new App();
		VM vm = new VM();
		try {
			String program = app.readFile(fileName);
			vm.execute(program);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CompileException e) {
			e.printStackTrace();
		}
	}

	
}
