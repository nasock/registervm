package vm;

import java.util.ArrayList;
import java.util.HashMap;

import instructions.*;
import instructions.flow.*;
import vm.Token.TokenType;

public class Compiler {
	private ArrayList<Token> tokens;
	private int tokenIndex;
	private VM vm;
	private ArrayList<Instruction> instructions;
	private HashMap<String, Integer> procsAddresses;

	public Compiler(VM aVm) {
		vm = aVm;
	}

	public HashMap<String, Integer> getProcsAddresses() {
		return procsAddresses;
	}

	private Token getToken() {
		return tokens.get(tokenIndex);
	}

	private void incrementToken() {
		tokenIndex++;
	}

	private void checkSize() throws CompileException {
		if (tokenIndex >= tokens.size()) {
			throw new CompileException("out of bounds");
		}
	}

	private boolean equalTokenType(TokenType type) {
		Token token = getToken();
		Token.TokenType curType = token.getType();
		return curType == type;
	}

	private void assertType(Token.TokenType type) throws CompileException {
		if (!equalTokenType(type)) {
			throw new CompileException("invalid token type " + getToken().getType());
		}
	}

	private void putLabelToMap() throws CompileException {
		Token token = getToken();
		String value = token.getValue();
		String prosidgerName = value.substring(0, value.length() - 1);
		procsAddresses.put(prosidgerName, instructions.size() - 1);
	}

	private String getLabel() throws CompileException {
		assertType(Token.TokenType.NAME);
		Token token = getToken();
		return token.getValue();
	}

	private void addCallInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionCall(label, line));
	}

	private void addBRKInstruction(int line) throws CompileException {
		instructions.add(new InstructionBRK(line));
	}

	private Register getRegister() throws CompileException {
		assertType(Token.TokenType.NAME);
		Token token = getToken();
		String letter = token.getValue();
		return vm.getRegister(letter);
	}

	private int getConstIndex() throws CompileException {
		Token token = getToken();
		String constStr = token.getValue();
		try {
			int index = vm.getConstIndex(constStr);
			return index;
		} catch (VMRuntimeException e) {
			throw new CompileException(e);
		}
	}

	private int getIntegerIndex() throws CompileException {
		assertType(Token.TokenType.INT);
		Token token = getToken();
		String intStr = token.getValue();
		int value = Integer.parseInt(intStr);
		return vm.putInt(value);
	}

	private int getIndex() throws CompileException {
		int index;
		if (equalTokenType(Token.TokenType.CONST)) {
			index = getConstIndex();
		} else {
			index = getIntegerIndex();
		}
		return index;
	}

	private void addMovInstruction(int line) throws CompileException {
		checkSize();
		Register reg1 = getRegister();
		incrementToken();
		if (equalTokenType(Token.TokenType.NAME)) {
			Register reg2 = getRegister();
			instructions.add(new InstructionMovReg(reg1, reg2, line));
		} else {
			int index = getIndex();
			instructions.add(new InstructionMovInt(reg1, index, line));
		}
	}

	private void addAddInstruction(int line) throws CompileException {
		checkSize();
		Register reg1 = getRegister();
		incrementToken();
		if (equalTokenType(Token.TokenType.NAME)) {
			Register reg2 = getRegister();
			instructions.add(new InstructionAddReg(reg1, reg2, line));
		} else {
			int index = getIndex();
			instructions.add(new InstructionAddInt(reg1, index, line));
		}
	}

	private void addSubInstruction(int line) throws CompileException {
		checkSize();
		Register reg1 = getRegister();
		incrementToken();
		if (equalTokenType(Token.TokenType.NAME)) {
			Register reg2 = getRegister();
			instructions.add(new InstructionSubReg(reg1, reg2, line));
		} else {
			int index = getIndex();
			instructions.add(new InstructionSubInt(reg1, index, line));
		}
	}

	private void addMulInstruction(int line) throws CompileException {
		checkSize();
		Register reg1 = getRegister();
		incrementToken();
		if (equalTokenType(Token.TokenType.NAME)) {
			Register reg2 = getRegister();
			instructions.add(new InstructionMulReg(reg1, reg2, line));
		} else {
			int index = getIndex();
			instructions.add(new InstructionMulInt(reg1, index, line));
		}
	}

	private void addDivInstruction(int line) throws CompileException {
		checkSize();
		Register reg1 = getRegister();
		incrementToken();
		if (equalTokenType(Token.TokenType.NAME)) {
			Register reg2 = getRegister();
			instructions.add(new InstructionDivReg(reg1, reg2, line));
		} else {
			int index = getIndex();
			instructions.add(new InstructionDivInt(reg1, index, line));
		}
	}

	private void addCMPInstruction(int line) throws CompileException {
		checkSize();
		Register reg1 = getRegister();
		incrementToken();
		if (equalTokenType(Token.TokenType.NAME)) {
			Register reg2 = getRegister();
			instructions.add(new InstructionCMPReg(reg1, reg2, line));
		} else {
			int index = getIndex();
			instructions.add(new InstructionCMPInt(reg1, index, line));
		}
	}

	private void addJMPInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJMP(label, line));
	}

	private void addJEQInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJEQ(label, line));
	}

	private void addJNEInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJNE(label, line));
	}

	private void addJGEInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJGE(label, line));
	}

	private void addJGTInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJGT(label, line));
	}

	private void addJLEInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJLE(label, line));
	}

	private void addJLTInstruction(int line) throws CompileException {
		checkSize();
		String label = getLabel();
		instructions.add(new InstructionJLT(label, line));
	}

	private void addOutInstruction(int line) throws CompileException {
		checkSize();
		Register reg = getRegister();
		instructions.add(new InstructionOut(reg, line));
	}

	private void addRetInstruction(int line) throws CompileException {
		instructions.add(new InstructionRet(line));
	}

	private void putIndexes() throws CompileException {
		for (Instruction instruction : instructions) {
			if (instruction instanceof FlowInstruction) {
				FlowInstruction flowInstruction = (FlowInstruction) instruction;
				String lable = flowInstruction.getLabel();
				Integer index = procsAddresses.get(lable);
				if (index == null) {
					throw new CompileException("no label " + lable);
				}
				flowInstruction.updateIndex(index);
			}
		}
	}

	private void putConstToTable() throws CompileException {
		Token token = getToken();
		String constant = token.getValue();
		incrementToken();
		assertType(Token.TokenType.INT);
		token = getToken();
		String intStr = token.getValue();
		int value = Integer.parseInt(intStr);
		vm.putConst(constant, value);
	}

	private void compileCode() throws CompileException {
		incrementToken();
		assertType(Token.TokenType.PROC);
		while (tokenIndex < tokens.size()) {
			Token token = getToken();
			TokenType type = token.getType();
			int line = token.getLine();
			if (type == Token.TokenType.SECTION) {
				incrementToken();
				break;
			} else if (type == Token.TokenType.PROC) {
				putLabelToMap();
			} else if (type == Token.TokenType.BRK) {
				addBRKInstruction(line);
			} else if (type == Token.TokenType.CALL) {
				incrementToken();
				addCallInstruction(line);
			} else if (type == Token.TokenType.MOV) {
				incrementToken();
				addMovInstruction(line);
			} else if (type == Token.TokenType.ADD) {
				incrementToken();
				addAddInstruction(line);
			} else if (type == Token.TokenType.SUB) {
				incrementToken();
				addSubInstruction(line);
			} else if (type == Token.TokenType.MUL) {
				incrementToken();
				addMulInstruction(line);
			} else if (type == Token.TokenType.DIV) {
				incrementToken();
				addDivInstruction(line);
			} else if (type == Token.TokenType.CMP) {
				incrementToken();
				addCMPInstruction(line);
			} else if (type == Token.TokenType.JMP) {
				incrementToken();
				addJMPInstruction(line);
			} else if (type == Token.TokenType.JEQ) {
				incrementToken();
				addJEQInstruction(line);
			} else if (type == Token.TokenType.JNE) {
				incrementToken();
				addJNEInstruction(line);
			} else if (type == Token.TokenType.JGE) {
				incrementToken();
				addJGEInstruction(line);
			} else if (type == Token.TokenType.JGT) {
				incrementToken();
				addJGTInstruction(line);
			} else if (type == Token.TokenType.JLE) {
				incrementToken();
				addJLEInstruction(line);
			} else if (type == Token.TokenType.JLT) {
				incrementToken();
				addJLTInstruction(line);
			} else if (type == Token.TokenType.OUT) {
				incrementToken();
				addOutInstruction(line);
			} else if (type == Token.TokenType.RET) {
				addRetInstruction(line);
			} else {
				throw new CompileException("invalid type in code");
			}
			incrementToken();
		}
		putIndexes();
	}

	private void compileData() throws CompileException {
		incrementToken();
		assertType(Token.TokenType.CONST);
		while (tokenIndex < tokens.size()) {
			TokenType type = getToken().getType();
			if (type == Token.TokenType.SECTION) {
				incrementToken();
				break;
			} else if (type == Token.TokenType.CONST) {
				putConstToTable();
			} else {
				throw new CompileException("invalid type in data");
			}
			incrementToken();
		}
	}

	public ArrayList<Instruction> compile(ArrayList<Token> aTokens) throws CompileException {
		tokens = aTokens;
		tokenIndex = 0;
		instructions = new ArrayList<Instruction>();
		procsAddresses = new HashMap<String, Integer>();

		assertType(Token.TokenType.SECTION);
		incrementToken();
		if (equalTokenType(Token.TokenType.CODE)) {
			int codeIndex = tokenIndex;
			while (tokenIndex < tokens.size() && !equalTokenType(Token.TokenType.DATA)) {
				incrementToken();
			}
			if (tokenIndex < tokens.size()) {
				compileData();
			}
			tokenIndex = codeIndex;
			compileCode();
		} else if (equalTokenType(Token.TokenType.DATA)) {
			compileData();
			if (tokenIndex < tokens.size()) {
				assertType(Token.TokenType.CODE);
				compileCode();
			}
		} else {
			throw new CompileException("invalid type");
		}
		return instructions;
	}

}
