package vm;

public abstract class Register {
	private int id;
	private String name;

	public Register(int aId, String aName) {
		id = aId;
		name = aName;
	}

	public abstract void store(int v);

	public void store(Register reg) {
		store(reg.retreive());
	}

	public abstract int retreive();

	public int getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Register)) {
			return false;
		}
		Register reg = (Register) obj;
		return reg.getId() == id;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public String toFullString() {
		return name + " : " + retreive();
	}
}
