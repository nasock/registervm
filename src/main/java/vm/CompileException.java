package vm;

public class CompileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CompileException(String cause) {
		super(cause);
	}

	public CompileException(Throwable e) {
		super(e);
	}

}
